# Advent of Code

My solutions in various languages. Mainly used to learn the languages, or new concepts in the languages I am already familiar with.

## Usage

### TypeScript

Move to the `typescript` directory. The main entrypoint is the `runner.ts` file.

To print both solutions for some dayXX (where XX is from 01-25), enter the following command:

```bash
# To run the example data input (data/dayXX.example.txt)
# Also, the Y in the `example` flag is for different example files
npx ts-node runner.ts dayXX -eY

# To run the real data
npx ts-node runner.ts dayXX
```
