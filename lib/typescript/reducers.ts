export const pipe = (...fns: any[]) => (input: any)  => fns.reduce((acc, fn) => fn(acc), input);

export const mapR = (fn: Function) => (input: any) => input.map(fn);

export const sum = (arr: number[]) => arr.reduce((acc, n) => acc + n, 0);
