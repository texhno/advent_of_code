export const concatStr = (str1: string, str2: string) => str1 + str2;

export const strToNum = (str: string) => parseInt(str, 10);

export const revStr = (str: string) => str.split("").reverse().join("");

type Lines = (str: string) => string[];
export const lines: Lines = (str) => str.split("\n");

export const blocks: Lines = (str) => str.split("\n\n");

export const getMax = (arr: number[]) => Math.max(...arr);

export const sortDesc = (arr: number[]) => arr.sort((a, b) => b - a);

export const take = (n: number) => (arr: any[]) => arr.slice(0, n);
