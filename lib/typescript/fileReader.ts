import fs from "fs";

type ReadFileToString = (fPath: string) => string;
export const readFIleToString: ReadFileToString = (fPath) =>
    fs.readFileSync(fPath).toString().trim();
