type ExeRegex = (reg: RegExp) => (str: string) => string;
export const exeRegex: ExeRegex = (reg) => (str) => reg.exec(str)![0].toString();

export const regDigits = /(?<=^\w*)(\d|one|two|three|four|five|six|seven|eight|nine)/i;

export const regDigitsReverse = /(?<=^\w*)(\d|eno|owt|eerht|ruof|evif|xis|neves|thgie|enin)/i;

export const firstDigitInStr = /(?<=^\w*)\d/i;

export const lastDigitInStr = /\d(?=\w*$)/i;
