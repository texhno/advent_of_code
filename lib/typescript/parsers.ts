import { F } from "./combinators";
import { pipe } from "./reducers";
import { concatStr, revStr, strToNum } from "./helpers";
import { exeRegex, regDigits, regDigitsReverse, firstDigitInStr, lastDigitInStr } from "./regExp";

const numbersMap = {
    "one": "1",
    "two": "2",
    "three": "3",
    "four": "4",
    "five": "5",
    "six": "6",
    "seven": "7",
    "eight": "8",
    "nine": "9",
};

const getIntValue = (str: string) =>
    parseInt(str) ? str : numbersMap[str as keyof typeof numbersMap];

type ExtractFLD = (str: string) => number;
export const extractFirstAndLastDigit: ExtractFLD = pipe(
    F(concatStr, exeRegex(firstDigitInStr), pipe(revStr, exeRegex(lastDigitInStr))),
    strToNum,
);

export const extractRealFirstAndLastDigit: ExtractFLD = pipe(
    F(concatStr,
        pipe(exeRegex(regDigits), getIntValue),
        pipe(pipe(revStr, exeRegex(regDigitsReverse)), revStr, getIntValue)),
    strToNum,
);
