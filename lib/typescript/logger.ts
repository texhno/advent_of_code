type Report = (r1: number, r2: number) => void;
export const report: Report = (res1, res2) => console.log(`
    Part1: ${res1}
    Part2: ${res2}
`);
