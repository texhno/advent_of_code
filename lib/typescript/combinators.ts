export const Y = () => 0;

export const F = (join: Function, fn1: Function, fn2: Function) => (val: any) => join(fn1(val), fn2(val));

export const C = (fn1: Function, fn2: Function) => (val: any) => fn1(fn2(val));

export const tap = (fn: Function) => (val: any) => {
    fn(val);
    return val;
};
