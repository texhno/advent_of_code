import { lines } from "../../../lib/typescript/helpers";
import { pipe, mapR, sum } from "../../../lib/typescript/reducers";
import { extractRealFirstAndLastDigit } from "../../../lib/typescript/parsers";

export const solve = pipe(
    lines,
    mapR(extractRealFirstAndLastDigit),
    sum,
);
