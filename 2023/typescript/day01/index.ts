import { solve as part1 } from "./d01p1";
import { solve as part2 } from "./d01p2";

export { part1, part2 };
