import { lines } from "../../../lib/typescript/helpers";
import { pipe, mapR, sum } from "../../../lib/typescript/reducers";
import { extractFirstAndLastDigit } from "../../../lib/typescript/parsers";

export const solve = pipe(
    lines,
    mapR(extractFirstAndLastDigit),
    sum,
);
