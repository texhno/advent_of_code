import { report } from "../../lib/typescript/logger";
import { readFIleToString as getInput } from "../../lib/typescript/fileReader";

const MIN_ARGS = 3;

(async () => {
    if (process.argv.length < MIN_ARGS) console.log(`Usage: cmd [fName]\nOr: cmd [fName] -e`);

    const { part1, part2 } = await import(`../typescript/${process.argv[2]}`);
    const input = getInput( `../data/${process.argv[2]}${/-e\d/gi.exec(process.argv[3]) ? `.example${process.argv[3][2]}` : ""}.txt`)
    
    report(part1(input), part2(input));
})();
