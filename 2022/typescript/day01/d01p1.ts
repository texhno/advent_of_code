import { mapR, pipe, sum } from "../../../lib/typescript/reducers";
import { blocks, lines, strToNum, getMax } from "../../../lib/typescript/helpers";

export const solve = pipe(
    blocks,
    mapR(lines),
    mapR(mapR(strToNum)),
    mapR(sum),
    getMax,
);
