import { mapR, pipe, sum } from "../../../lib/typescript/reducers";
import { blocks, lines, strToNum, sortDesc, take } from "../../../lib/typescript/helpers";

export const solve = pipe(
    blocks,
    mapR(lines),
    mapR(mapR(strToNum)),
    mapR(sum),
    sortDesc,
    take(3),
    sum,
);
